##  MOEX signals analyses

---

**Конфигурация:**

- [configurations](./config.ini)
- [dependencies](./requirements.txt)

---

**Запуск:**

    run_app.py


**Параметры запуска (True or False):**

    update_all          -> load all data from ISS and updete all tables
    update_last_year    -> load last year in ISS and replace this year in DB
    update_i3           -> load data to Database
    graphs              -> save graphs

---

**Прочие параметры:**

- [Запуск приложения](app/run.py)
- [Доп. конфигурация](./config.py)