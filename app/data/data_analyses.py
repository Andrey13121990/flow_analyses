import os
import pandas as pd
import numpy as np
from typing import Union
from time import time


from .data_from_db import DataLoader


class DataAggregator:

    def __init__(self, tickers):
        self.Loader = DataLoader(tickers)
        self.price_field = 'close'
        self.indicator_1 = None
        self.indicator_2 = None
        self.mov_avg = 20

        self.tickers = tickers
        self.data = self.cache_data()


    def set_indicator_1(self, indicator):
        self.indicator_1 = indicator


    def set_indicator_2(self, indicator):
        self.indicator_2 = indicator


    def get_indicator(self, ticker: str, indicator: str):
        return self.data[ticker][indicator].to_frame()

    
    def get_pivot(self):
        df = self.Loader.get_pivot()
        return df

    
    def cache_data(self):
        data = dict()
        df = self.get_pivot()
        for ticker in self.tickers:
            df_ticker = df[df['ticker'] == ticker].copy()
            df_ticker['yield'] = df_ticker[self.price_field].pct_change() * 100
            data[ticker] = df_ticker
        
        data[''] = pd.DataFrame(columns=data['SBER'].columns)
        return data


    # ---------------- For Histogram ----------------

    def indicators_hist(self, ticker: str, indicator: str):
        df = self.data[ticker]
        return df[indicator].to_frame().dropna()


    # ---------------- For Scatter ----------------

    def indicators_scatter(self, ticker: str):
        df = self.data[ticker]   
        return df[[self.indicator_1, self.indicator_2, 'yield']].dropna()


    # ---------------- For Correlation (in different windows) ----------------

    def day_groupping(self, df, column: str, function: str, step: int):
        
        if function == 'sum':
            return df[column].rolling(window=step + 1).sum()

        if function == 'last':
            return df[column].pct_change(periods = step + 1)


    def moving_window_df(self, ticker: str, step: int):

        df = pd.DataFrame()
        ticker_df = self.data[ticker]
        df[self.indicator_1] = self.day_groupping(ticker_df, self.indicator_1, 'sum', step)
        df[self.indicator_2] = self.day_groupping(ticker_df, self.indicator_2, 'sum', step)
        df['yield'] = self.day_groupping(ticker_df, self.price_field, 'last', step)

        return df


    def calculate_correlation(self, ticker: str, step: int):
        
        df = self.moving_window_df(ticker, step)
        result = df.corr().iloc[2:,:2]
        result.reset_index(inplace = True)
        del result['index']

        return result


    def get_corr_window(self, ticker: str):
        
        df = self.calculate_correlation(ticker, 0)
        step = 10
        end = 200
        index = [1]
        for item in range(10, end+1, step):
            data = self.calculate_correlation(ticker, item)
            df = pd.concat([df, data], ignore_index = True)
            index.append(item+1)
        
        if ticker != '':
            df.index = index
        return df


    # ---------------- For Heatmap ----------------
        
    def get_indicators_corr(self, indicator: str):
        
        df = pd.DataFrame()
        for ticker in self.tickers:
            data = self.get_indicator(ticker, indicator).dropna()
            df[ticker] = data[indicator]
            df = df.dropna(1, how = 'all')
        return df.corr()


    # ---------------- For Lines ----------------

    def get_cumulative(self, ticker: str):
        df = self.data[ticker].copy()

        df['netto_1'] = df[self.indicator_1].cumsum()
        df['netto_2'] = df[self.indicator_2].cumsum()

        df['mov_avg_1'] = df['netto_1'].rolling(window=self.mov_avg).mean()
        df['mov_avg_2'] = df['netto_2'].rolling(window=self.mov_avg).mean()

        df[self.indicator_1] = df['mov_avg_1'] / 1000
        df[self.indicator_2] = df['mov_avg_2'] / 1000

        return df[[self.indicator_1, self.indicator_2]].dropna()

    
    def get_close_price(self, ticker: str):
        df = self.data[ticker].copy()

        df[self.price_field] = df[self.price_field].rolling(window=self.mov_avg).mean()
        return df[self.price_field ].to_frame().dropna()



class DataToGraphs(DataAggregator):

    def __init__(self, tickers):
        super().__init__(tickers)


    def complex_graph(self, *args: str, function, **kwargs: str):
        tickers = []
        dfs = []
        for arg in args:
            tickers.append(arg)
            dfs.append(function(arg, **kwargs))
        
        return (tickers, dfs)


    def ind_complex_scatter(self, *args: str) -> tuple:
        return self.complex_graph(*args, function = self.indicators_scatter)


    def ind_complex_hist(self, *args: str, indicator: str):
        return self.complex_graph(*args, function = self.indicators_hist, indicator = indicator)


    def correlation_complex(self, *args: str) -> tuple:
        return self.complex_graph(*args, function = self.get_corr_window)


    def price_complex(self, *args: str) -> tuple:
        return self.complex_graph(*args, function = self.get_close_price)


    def flow_cum_complex(self, *args: str) -> tuple:
        return self.complex_graph(*args, function = self.get_cumulative)