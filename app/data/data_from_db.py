import os
from pandas import read_sql
from typing import Union
from config import ConfigDB, ConfigDirectories


from .db import DatabaseConnection


class SQLqueries():

    def get_quotes(self, ticker: str, **kwargs) -> str:
        sql = """
            SELECT ticker, date, close, value
            FROM {table}
            WHERE ticker LIKE '{ticker}'
            """

        return sql.format(ticker = ticker, 
                            table = ConfigDirectories().quotes)


    def get_indicators(self, ticker: str, indicator: str, **kwargs) -> str:
        sql = """
            SELECT date, {indicator}
            FROM {table}
            WHERE ticker LIKE '{ticker}'
            """

        return sql.format(ticker = ticker, 
                            indicator = indicator, 
                            table = ConfigDirectories().indicators)


    def get_ticker_list(self, **kwargs) -> str:
        sql = """
            SELECT ticker
            FROM {view}
            WHERE ticker NOT LIKE 'eq'
            GROUP BY ticker
            """
        return sql.format(view = ConfigDB().view_name)

    
    def get_pivot(self, ticker_list, **kwargs) -> str:
        sql = """
            SELECT {table}.ticker, 
                    {table}.date, 
                    {table}.close, 
                    {table}.value,
                    {view}.i3,
                    {view}.i3_retail,
                    {view}.pv30,
                    {view}.pv70,
                    {view}.pv100
            FROM {table}
            INNER JOIN {view} ON {table}.ticker = {view}.ticker and
                                        {table}.date = {view}.date
            WHERE {table}.ticker IN {ticker_list}
            ORDER BY {table}.date ASC
            """

        return sql.format(ticker_list = tuple(ticker_list), 
                            view = ConfigDB().view_name, 
                            table = ConfigDirectories().quotes)



class DataLoader:

    def __init__(self, ticker_list):
        self.SQL = SQLqueries()
        self.session = DatabaseConnection().open_connection()
        self.ticker_list = ticker_list
        self.date_columns = ['date']
        self.index = 'date'
        self.columns_to_transform = ['value', 'i3', 'i3_retail', 'pv30', 'pv100', 'pv70']
        self.transform_digit = 1E6


    def get_data(self, 
                func, 
                ticker: Union[None, str] = None,
                ticker_list: Union[None, list] = None,
                indicator: Union[None, str] = None,
                parse_dates: Union[None, list] = None):
        
        sql = func(ticker = ticker, indicator = indicator, ticker_list = ticker_list)
        df = read_sql(sql, con = self.session, parse_dates = parse_dates)
        return df


    def transform_to_date(self, df):
        for column in self.date_columns:
            df[column] = df[column].dt.date        
        return df


    def change_digit(self, df): 
        for column in df.columns:
            if column in self.columns_to_transform:
                df[column] = df[column] / self.transform_digit   
        return df


    def normalize_data(self, df):
        df = self.transform_to_date(df)
        df = self.change_digit(df)
        df.set_index(self.index, inplace = True)
        return df


    def get_tickers(self):
        return self.get_data(func = self.SQL.get_ticker_list)


    def get_pivot(self):
        df = self.get_data(func = self.SQL.get_pivot,
                            ticker_list = self.ticker_list,
                            parse_dates = self.date_columns)

        return self.normalize_data(df)


    def get_indicator(self, ticker: str, indicator: str):
        df = self.get_data(func = self.SQL.get_indicators, 
                            ticker = ticker, 
                            indicator = indicator, 
                            parse_dates = self.date_columns)

        return self.normalize_data(df)