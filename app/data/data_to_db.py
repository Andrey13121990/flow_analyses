import os
import pandas as pd
import datetime
from config import ConfigDB, ConfigDirectories

from .db import DatabaseConnection



class DataReader:

    def __init__(self, directory: str):
        self.directory = directory
        self.files = self.return_paths()


    def return_files(self):
        path = os.path.join(os.getcwd(), self.directory)
        return os.listdir(path)


    def return_ticker(self, file: str):
        return file[:-9]


    def return_paths(self):
        path = os.path.join(os.getcwd(), self.directory)
        files = self.return_files()
        
        results = []
        for file in files:
            file_path = os.path.join(path, file)
            ticker = self.return_ticker(file)
            data = (file_path, ticker)
            results.append(data)

        return results


    def normalize_quotes(self, df, ticker: str):
        df['tradedate'] = pd.to_datetime(df['begin'], format = '%Y-%m-%d')
        df['date'] = df['tradedate'].dt.date
        df['ticker'] = ticker
        
        del df['begin']
        del df['end']
        del df['tradedate']


    def normalize_indicators(self, df):
        df['tradedate'] = pd.to_datetime(df['tradedate'], format = '%Y-%m-%d')
        df['date'] = df['tradedate'].dt.date

        del df['tradedate']


    def normalize_netflow(self, df):
        df['date'] = pd.to_datetime(df['date'], format = '%Y-%m-%d')
        df['date'] = df['date'].dt.date


    def read_data(self, path: str, ticker: str):

        if self.directory == ConfigDirectories().quotes:
            df = pd.read_csv(path, sep = ';', skiprows=1)
            self.normalize_quotes(df, ticker)

        if self.directory == ConfigDirectories().indicators:
            df = pd.read_csv(path, sep = ',')
            self.normalize_indicators(df)

        if self.directory == ConfigDirectories().netflow:
            df = pd.read_csv(path, sep = ';', skiprows=1)
            self.normalize_netflow(df)

        return df



class DatabsaseLoader:

    def __init__(self):
        self.session = DatabaseConnection().open_connection()


    def make_view(self) -> str:

        sql = """
            CREATE VIEW {view} AS 

            SELECT  {table_1}.date, 
                    {table_1}.ticker, 
                    {table_1}.netto_rub as i3,
                    {table_1}.retail_netto_rub as i3_retail,
                    {table_2}.pv30,
                    {table_2}.pv70,
                    {table_2}.pv100
            FROM {table_1}
            LEFT JOIN {table_2} ON {table_1}.ticker = {table_2}.ticker and
                                    {table_1}.date = {table_2}.date
            UNION
            SELECT  {table_2}.date, 
                    {table_2}.ticker, 
                    {table_1}.netto_rub as i3,
                    {table_1}.retail_netto_rub as i3_retail,
                    {table_2}.pv30,
                    {table_2}.pv70,
                    {table_2}.pv100
            FROM {table_1}
            RIGHT JOIN {table_2} ON {table_1}.ticker = {table_2}.ticker and
                                    {table_1}.date = {table_2}.date  
            """

        return sql.format(view = ConfigDB().view_name, 
                            table_1 = ConfigDirectories().indicators, 
                            table_2 = ConfigDirectories().netflow)


    def truncate_table(self, table: str) -> None:
        sql = """
            TRUNCATE TABLE {table}
            """
        return sql.format(table = table)


    def delete_last_year(self, table: str):
        today = datetime.datetime.today() 
        
        sql = """
            DELETE FROM {table}
            WHERE DATE >= TO_DATE('01-01-{year}', 'DD-MM-YYYY')
            """
        return sql.format(table = table, year = str(today.year))


    def update_all(self, directory: str):
        DR = DataReader(directory)

        try:
            sql = self.truncate_table(directory)
            self.session.execute(sql) 
            print ('Table %s trancated' % directory)
        except:
            pass

        for file in DR.files:
            df = DR.read_data(path = file[0], ticker = file[1])
            df.to_sql(directory, con = self.session, if_exists = 'append')
        
        print('Table %s updated all\n' % directory)


    def update_last_year(self, directory: str):
        DR = DataReader(directory)
        
        sql = self.delete_last_year(directory)
        self.session.execute(sql) 
        print ('Table %s deleted last year' % directory)

        for file in DR.files:
            df = DR.read_data(path = file[0], ticker = file[1])
            df.to_sql(directory, con = self.session, if_exists = 'append')
        
        print('Table %s updated last year\n' % directory)


    def create_view(self):
        try:
            sql = self.make_view()
            self.session.execute(sql)
        except:
            pass


    def load_data(self, period: str, directories: list):
        
        for directory in directories:
            
            if period == 'all':
                self.update_all(directory)

            elif period == 'last_year':
                self.update_last_year(directory)
            
            else:
                pass
        
        self.create_view()

    


