from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from config import ConfigDB



class DatabaseConnection:

    def __init__(self):
        self.db = ConfigDB().db
        self.login = ConfigDB().login
        self.password = ConfigDB().password
        
        

    def make_exadata_session(self):

        dnsstr = ConfigDB().EXADATA_URI

        engine = create_engine('oracle+cx_oracle://'+ self.login + ':' + self.password + '@'+ dnsstr)
        Session = sessionmaker(bind=engine)
        return Session()


    def make_postgres_connection(self):
        uri = ConfigDB().POSTGRES_URI
        engine = create_engine(uri, echo = False)
        
        return engine


    def open_connection(self):
        
        if self.db == 'EXADATA':
            return self.make_exadata_session()
        
        elif self.db == 'POSTGRES':
            return self.make_postgres_connection()

        else: 
            raise Exception ('Choose EXADATA or POSTGRES database in config file')
            