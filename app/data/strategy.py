import pandas as pd
import numpy as np

from scipy.signal import argrelextrema
from .data_analyses import DataAggregator
from config import CONFIG_STRATEGY_CLOBAL


class Strategy:

    def __init__(self, ticker, aggregated_data):
        self.ticker = ticker
        self.aggregated_data = aggregated_data
        self.indicator = CONFIG_STRATEGY_CLOBAL.parametres[ticker]['ind']
        self.order = CONFIG_STRATEGY_CLOBAL.parametres[ticker]['order']
        self.shift = CONFIG_STRATEGY_CLOBAL.parametres[ticker]['shift']
        
        self.data = self.get_data()
        self.extremums = self.get_extremums()


    def get_data(self):
        data = self.aggregated_data[self.ticker]
        return data[[self.indicator, 'yield']]


    def get_cumulative_flow(self):
        df = self.data
        data = df[self.indicator].cumsum()
        return data.dropna()


    def get_cumulative_flow_shift(self):
        return self.get_cumulative_flow().shift(self.shift).dropna()


    def find_local_max_and_min(self, data):
        min_data = data.iloc[argrelextrema(data.values, np.less, order=self.order)[0].tolist()]
        max_data = data.iloc[argrelextrema(data.values, np.greater, order=self.order)[0].tolist()]
        return (min_data, max_data)
    
    
    def get_extremums(self):
        data = self.get_cumulative_flow_shift()
        return self.find_local_max_and_min(data)


    def delete_dublicates(self, data):
        previouse = None
        result = []
        for item in data:
            if item == previouse:
                result.append(np.NaN)
            else:
                result.append(item)               
            previouse = item
        return result


    def prepare_signals(self):

        df = self.data[['yield']].copy()
        df['min'], df['max'] = self.extremums

        local_min = list(map(lambda x: 1 if x!=0 else int(x) , df['min'].fillna(0)))
        local_max = list(map(lambda x: -1 if x!=0 else int(x) , df['max'].fillna(0)))

        df['min'], df['max'] = local_min, local_max
        df['signal'] = df['min'] + df['max']
        del df['max']
        del df['min']

        return df

    
    def get_signal(self):
        df = self.prepare_signals()
        ind = df.index[self.order]

        results = df['signal'].loc[ind:]
        results = results[results != 0].to_frame()
        results['signal'] = self.delete_dublicates(results['signal'])
        return results.dropna()


    def get_performance(self):

        df = self.prepare_signals()

        df['signal'] = self.get_signal()
        
        df['long_short'] = df['signal'].fillna(method='ffill')
        df['long'] = list(map(lambda x: int(x) if x==1 else 0 , df['long_short']))
        start = len(df['long_short']) - len(df['long_short'].dropna()) 

        df['long_short_pl'] = df['long_short'] * df['yield'] / 100 + 1
        df['long_short_pl'] = df['long_short_pl'].cumprod()

        df['long_pl'] = df['long'] * df['yield'] / 100 + 1
        df['long_pl'] = df['long_pl'].cumprod()

        df['base'] = df['yield'] / 100 + 1
        df['base'] = df['base'].iloc[start:,].cumprod()

        return df[['long_short_pl', 'long_pl', 'base']].dropna()



class Backtest(Strategy):
    def __init__(self, ticker, aggregated_data):
        super().__init__(ticker, aggregated_data) 
        self.order_min = CONFIG_STRATEGY_CLOBAL.parametres[ticker]['ord_min']
        self.order_max = CONFIG_STRATEGY_CLOBAL.parametres[ticker]['ord_max']
        self.shift_min = CONFIG_STRATEGY_CLOBAL.parametres[ticker]['shift_min']
        self.shift_max = CONFIG_STRATEGY_CLOBAL.parametres[ticker]['shift_max']
        self.mov_avg = CONFIG_STRATEGY_CLOBAL.parametres[ticker]['mov_avg']
        
        self.extremums = self.get_extremums_strat()


    def get_cumulative_flow_mov_avg(self):
        df = self.data
        data = df[self.indicator].cumsum()

        return data.rolling(self.mov_avg).mean().dropna()


    def find_local_extremums(self, data):
        min_data = data.iloc[argrelextrema(data.values, np.less, order=self.order_min)[0].tolist()]
        max_data = data.iloc[argrelextrema(data.values, np.greater, order=self.order_max)[0].tolist()]

        return (min_data, max_data)


    def get_max_and_min(self):
        data = self.get_cumulative_flow_mov_avg()
        res_min, res_max = [], []

        for item in range(len(data)):

            min_data, max_data = self.find_local_extremums(data.iloc[0:item])
        
            if list(min_data.values) == []:
                res_min.append(np.NAN)
            else:
                res_min.append(min_data[-1])

            if list(max_data.values) == []:
                res_max.append(np.NAN)
            else:
                res_max.append(max_data[-1])

        res_min = pd.Series(res_min)
        res_min.index = data.index

        res_max = pd.Series(res_max)
        res_max.index = data.index

        return (res_min[self.order:], res_max[self.order:])


    def get_max_and_min_shift(self):
        min_data, max_data = self.get_max_and_min()

        df_min = min_data.to_frame()
        df_min.columns = ['min']
        df_min['shift_min'] = df_min['min'].shift(self.shift_min)

        df_max = max_data.to_frame()
        df_max.columns = ['max']
        df_max['shift_max'] = df_max['max'].shift(self.shift_max)

        return (df_min, df_max)


    def get_signal_strat(self, df):
        df = df.dropna().copy()
        col_1, col_2 = df.columns[0], df.columns[1]
        
        result = []
        for a, b in zip(df[col_1], df[col_2]):
            
            if a == b:
                result.append(a)        
            else:
                result.append(np.NaN)
        df['signal'] = result

        return df


    def get_signal_cleaned(self, df):
        data = self.get_signal_strat(df).dropna()
        data['signal_2'] = self.delete_dublicates(data['signal'])
        
        return data.dropna()['signal_2']


    def get_extremums_strat(self):
        min_data, max_data = self.get_max_and_min_shift()
        local_min = self.get_signal_cleaned(min_data).drop_duplicates()
        local_max = self.get_signal_cleaned(max_data).drop_duplicates()

        return (local_min, local_max)



class StrategyToGraph:

    def __init__(self, ticker, aggregated_data):
        self.STR = Strategy(ticker, aggregated_data)
        self.BACK = Backtest(ticker, aggregated_data)
        self.table_list = self.get_table_list()
        self.title_list = self.get_title_list()
        self.table_list_2 = self.get_table_list_2()
        self.table_list_3 = self.get_table_list_3()
    

    def get_table_list(self):
        data = [self.STR.get_cumulative_flow().to_frame(), 
                self.BACK.get_cumulative_flow_mov_avg().to_frame(), 
                self.STR.get_performance(),
                self.BACK.get_performance()]

        return data


    def get_title_list(self):
        data = ['Локальные экстремумы, %s cumulative' % self.STR.indicator, 
                'Локальные экстремумы, %s cum moving avg' % self.STR.indicator, 
                'Теоретическая доходность', 
                'Практическая доходность']
        
        return data

    def get_table_list_2(self):
        min_data, max_data = self.STR.extremums
        
        return [min_data.to_frame(), max_data.to_frame()]
    

    def get_table_list_3(self):
        min_d, max_d = self.BACK.extremums

        return [min_d.to_frame(), max_d.to_frame()]