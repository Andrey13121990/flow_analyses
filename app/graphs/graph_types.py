import numpy as np
import pandas as pd
import warnings 
from typing import Union


class Graph:

    def __init__(self):
        self.colors = ['#CE1126', '#0070c0', '#FFA100','#53682B', '#63B1E5']
        self.add_colors = ['#2c3136', '#53682B', '#63B1E5']
        self.lines = ['-', '-', '-', '--', '--']

    
class Bar(Graph):

    def template_1(self, df, ax, colors):
        ind = np.arange(len(df))  
        col = df.columns
        bottom = df[col[0]] - df[col[0]]

        for item, color in zip(range(len(col)), colors):
            try:
                ax.bar(ind, 
                        df[col[item]], 
                        width = 0.7, 
                        color=color, 
                        edgecolor = 'white', 
                        bottom = bottom)         
                bottom += df[col[item]]
            except:
                pass


class Line(Graph):

    def template_1(self, df, ax, colors):
        ind = np.arange(len(df))  
        col = df.columns       

        for item, color, line in zip(range(len(col)), colors, self.lines):  
            try:
                ax.plot(ind,
                        df[col[item]], 
                        marker = 'o', 
                        color = color, 
                        linestyle = line)
            except:
                pass


    def template_2(self, df, ax, colors): 
        col = df.columns
        ind = pd.to_datetime(df.index)  

        for item, color, line in zip(range(len(col)), colors, self.lines):  
            try:
                ax.plot(ind,
                        df[col[item]], 
                        color = color, 
                        linestyle = line,
                        linewidth = 1.5)
            except:
                pass


class Hystogram(Graph):

    def template_1(self, df, ax, colors):
        col = df.columns

        for item, color in zip(range(len(col)), colors):
            try:
                ax.hist(np.array(df[col[item]]), 
                        bins = 25, 
                        color = color, 
                        edgecolor = 'white')
            except:
                pass


class Scatter(Graph):

    def add_trendline(self, x, y):
        z = np.polyfit(x, y, 1)
        p = np.poly1d(z)
        return p(x)


    def template_1(self, df, ax, colors):
        col = df.columns

        for item, color, line in zip(range(len(col)-1), colors, self.lines):
            try:
                ox = np.array(df[col[item]])
                oy = np.array(df[col[-1]])

                ax.scatter(ox,
                        oy, 
                        s = 15, 
                        color = color, 
                        alpha = 0.95,
                        edgecolor = 'white')

                trend = self.add_trendline(ox, oy)
                ax.plot (ox,
                        trend,
                        color = color, 
                        linestyle = line,
                        linewidth = 1.0)      
            except:
                pass


    def template_2(self, df, ax, colors):
        for data, color in zip(df, colors):
            try:
                ox = data.index
                oy = data[data.columns[0]]

                ax.scatter(ox,
                        oy, 
                        s = 150, 
                        alpha = 0.95,
                        color = color,
                        edgecolor = 'white')
            except:
                pass


class Heatmap(Graph):

    def template_1(self, df, ax, colors):
        ax.imshow(df.values)
        ax.set_xticks(np.arange(len(df.columns)))
        ax.set_yticks(np.arange(len(df.index)))

        ax.set_xticklabels(list(df.columns))
        ax.set_yticklabels(list(df.index), size = 8)

        for i in range(len(df.columns)):
            for j in range(len(df.index)):
                data = round(df.iloc[i, j], 1)
                ax.text(j, i, data,
                            ha="center", 
                            va="center", 
                            color="w",
                            size=5)
        
        ax.set_ylim((-0.5, len(df.index)-0.5))
        ax.grid(None)


class ScatterLine:

    def template_1(self, dfs, ax, colors):
        df_line, df_scatter = dfs[0], dfs[1]
        Line().template_2(df_line, ax, colors)
        Scatter().template_2(df_scatter, ax, colors)