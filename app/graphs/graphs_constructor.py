import matplotlib.pyplot as plt
from typing import Union

from .graphs_templates import GraphTemplate

plt.rcParams.update({'figure.max_open_warning': 0})
plt.style.use('bmh')



class SimpleGraph:

    def __init__(self):
        self.colors = ['#CE1126', '#0070c0', '#FFA100','#53682B', '#63B1E5']
        self.add_colors = ['#5a636e', '#53682B', '#63B1E5']
        self.lines = ['-', '-', '-', '--', '--']


    def set_colors(self, *args):
        new_colores = []
        
        for arg in args:
            new_colores.append(arg)
            
        self.colors = new_colores


    def set_add_colors(self, *args):
        new_colores = []
        
        for arg in args:
            new_colores.append(arg)
            
        self.add_colors = new_colores


    def set_lines(self, *args):
        new_lines = []
        
        for arg in args:
            new_lines.append(arg)
            
        self.lines = new_lines


    def graph_one_ax(self, 
                    df,
                    add_legend = False,
                    **kwargs) -> plt:
        
        if add_legend:
            plt.rcParams['figure.figsize'] = (7, 7)
        else:
            plt.rcParams['figure.figsize'] = (7, 5)
            
        fig = plt.figure()

        GT = GraphTemplate()
        GT.colors = self.colors
        GT.add_colors = self.add_colors
        GT.lines = self.lines
        try:
            GT.graph_format(fig = fig, 
                            df = df, 
                            add_legend = add_legend,
                            **kwargs)
        except:
            pass  
              
        fig.tight_layout()
        
        return plt



class ComplexGraph (SimpleGraph):
    def __init__(self) -> None:
        super().__init__()


    def data_to_subplots(self,
                        data: list,
                        ax_nums: int) -> list:
        result = []
        for item in range(ax_nums):
            result.append(data[item])
        
        return result
    
    
    def graph_one_ax(self, 
                    table_list: list,
                    title_list: list,
                    ax_nums: int = 4,
                    **kwargs) -> plt:

        plt.rcParams['figure.figsize'] = (14, 8)
        fig = plt.figure()

        GT = GraphTemplate()
        GT.colors = self.colors
        GT.add_colors = self.add_colors
        GT.lines = self.lines

        if ax_nums == 4:
            places = [221, 222, 223, 224]
        elif ax_nums == 6:
            places = [231, 232, 233, 234, 235, 236]

        dfs    = self.data_to_subplots(table_list, ax_nums)
        titles = self.data_to_subplots(title_list, ax_nums)

        for df, place, title in zip(dfs, places, titles):  
            try:
                GT.graph_format(fig = fig, 
                                df = df, 
                                place = place,                            
                                add_title = title,
                                **kwargs)        
            except:
                pass

        fig.tight_layout()
        fig.subplots_adjust(wspace=0.3, hspace=0.5)
        return plt 


    def graph_two_axes(self,
                    table_list_ax1: list,
                    table_list_ax2: list,
                    title_list: list,
                    ax_nums: int = 4,
                    **kwargs) -> plt:

        plt.rcParams['figure.figsize'] = (14, 8)
        fig = plt.figure()

        GT = GraphTemplate()
        GT.colors = self.colors
        GT.add_colors = self.add_colors
        GT.lines = self.lines

        if ax_nums == 4:
            places = [221, 222, 223, 224]
        elif ax_nums == 6:
            places = [231, 232, 233, 234, 235, 236]

        dfs    = self.data_to_subplots(table_list_ax1, ax_nums)
        dfs2   = self.data_to_subplots(table_list_ax2, ax_nums)
        titles = self.data_to_subplots(title_list, ax_nums)

        for df, df2, place, title in zip(dfs, dfs2, places, titles):
            try:
                GT.graph_format(fig = fig, 
                                df = df, 
                                df2 = df2,
                                place = place,
                                add_title = title,
                                **kwargs)
            except:
                pass

        fig.tight_layout()
        fig.subplots_adjust(wspace=0.3, hspace=0.5)
        
        return plt



class CustomGraph (SimpleGraph):
    def __init__(self) -> None:
        super().__init__()

    def graph_for_backtest(self, 
                    table_list: list,
                    table_list_2: list,
                    table_list_3: list,
                    title_list: list,
                    **kwargs) -> plt:

        plt.rcParams['figure.figsize'] = (14, 8)
        fig = plt.figure()

        GT = GraphTemplate()
        GT.colors = self.colors
        GT.add_colors = self.add_colors
        GT.lines = self.lines

        places = [221, 222, 223, 224]
        dfs    = table_list
        dfs_2  = table_list_2
        dfs_3  = table_list_3
        titles = title_list


        GT.graph_format(fig = fig, 
                        df = (dfs[0], dfs_2),
                        place = places[0],                            
                        add_title = titles[0],
                        main_graph = 'line_scatter',
                        **kwargs)        
        

        GT.graph_format(fig = fig, 
                        df = (dfs[1], dfs_3),
                        place = places[1],                            
                        add_title = titles[1],
                        main_graph = 'line_scatter',
                        **kwargs)   


        GT.graph_format(fig = fig, 
                        df = dfs[2], 
                        place = places[2],      
                        add_title = titles[2],
                        main_graph = 'line_price',
                        **kwargs)  

        GT.graph_format(fig = fig, 
                        df = dfs[3],
                        place = places[3],                            
                        add_title = titles[3],
                        main_graph = 'line_price',
                        **kwargs)   

        fig.tight_layout()
        fig.subplots_adjust(wspace=0.3, hspace=0.5)
        return plt 