import os
from time import time

from .graphs_paint import GraphPainter
from config import ConfigDirectories, CONFIG_STRATEGY_CLOBAL
from app.structure.directories import Folders


def timeit(graph_type):
    def decorator(func):
        def wrapper(self, *args, **kwargs):
            t0 = time()
            func(self, *args, **kwargs)
            t1 = time() - t0
            print('ok --- %s --- %s sec.' % (graph_type, round(t1, 1)))
        return wrapper
    return decorator


class SaveGraphs:
    def __init__(self, indicator_1, indicator_2, tickers):
        self.tickers = tickers
        self.indicator_1 = indicator_1
        self.indicator_2 = indicator_2
        self.path = self.get_path()
        self.subfolder = self.indicator_1 + '_and_' + self.indicator_2
        self.clear_dir()
        self.make_dirs()
        self.Graph = GraphPainter(self.path_results(), self.indicator_1, self.indicator_2, self.tickers)
        

    def get_path(self) -> str:
        path = os.path.join(os.getcwd(), ConfigDirectories().results)
        return path


    def make_dirs(self): 
        Folders().make_dir(self.subfolder , path = self.path)


    def clear_dir(self) -> None:
        Folders().clear_dir(self.subfolder, path = self.path)

    
    def path_results(self):
        return os.path.join(self.path, self.subfolder)

    
    def choose_tickers(self, factor):
        result = []
        tickers = self.tickers.copy()

        if len(tickers) % factor != 0:
            add = factor - len(tickers) % factor

            for item in range(add):
                tickers.append('')

        for item in range(len(tickers) // factor):
            data = tickers[item * factor: (item + 1) * factor]
            result.append(data)
            
        return result


    @timeit('histogram graphs')
    def save_hist(self):
        for num, ticker in enumerate(self.choose_tickers(6)):
            self.Graph.indicators_hist('%s_hist_%s' % (self.indicator_1, num),
                                        *ticker, 
                                        indicator = self.indicator_1) 

            self.Graph.indicators_hist('%s_hist_%s' % (self.indicator_2, num),
                                        *ticker, 
                                        indicator = self.indicator_2,
                                        change_color = True)                             


    @timeit('scatter graphs')
    def save_scatter(self):
        for num, ticker in enumerate(self.choose_tickers(6)):
            self.Graph.indicators_scatter('%s_scatter_%s' % (self.indicator_1, num), *ticker) 


    @timeit('correlation graphs')
    def save_correlation(self):
        for num, ticker in enumerate(self.choose_tickers(6)):
            self.Graph.correlations('%s_correlation_%s' % (self.indicator_1, num), *ticker)                                


    @timeit('correlation heatmap')
    def save_corr_heatmap(self):
        self.Graph.heatmap_corr('%s_corr_heatmap' % self.indicator_1, indicator = self.indicator_1) 
        self.Graph.heatmap_corr('%s_corr_heatmap' % self.indicator_2, indicator = self.indicator_2) 


    @timeit('price graphs')
    def save_cum_flow(self):
        for num, ticker in enumerate(self.choose_tickers(4)):
            self.Graph.indicators_cum('%s_cum_flow_%s' % (self.indicator_1,num), *ticker) 
           

    @timeit('backtest graphs')
    def save_backtest(self): 
        if self.indicator_1 == 'pv100' or self.indicator_2 == 'pv100':
            self.subfolder = 'strategy'
            self.clear_dir()
            self.make_dirs()
            self.Graph.set_path(self.path_results())

            for ticker in self.tickers:
                t0 = time()
                self.Graph.backtest('backtest %s' % ticker, ticker)
                t1 = time() - t0
                print('backtest - %s - %s sec.' % (ticker, round(t1, 1)))


    @timeit('backtest new version')
    def select_backtest_parametres(self, version):
        if self.indicator_1 == 'pv100' or self.indicator_2 == 'pv100':
            self.subfolder = os.path.join('strategy_selections', 'version %s' % version)
            self.clear_dir()
            self.make_dirs()
            self.Graph.set_path(self.path_results())

            for ticker in self.tickers:
                self.Graph.backtest('backtest %s' % ticker, ticker)
                print('%s --- backtest --- version %s' % (ticker, version))


    def save_backtest_selection(self):
        parametres = CONFIG_STRATEGY_CLOBAL.parametres_dev()
        for version in parametres.keys():
            CONFIG_STRATEGY_CLOBAL.set_parametres_development(version)
            self.select_backtest_parametres(version)


    def save(self, is_production):
        print('%s / %s indicators' % (self.indicator_1, self.indicator_2))

        if is_production:
            self.save_cum_flow()
            self.save_hist()
            self.save_scatter()
            self.save_correlation()
            self.save_corr_heatmap()
            self.save_backtest()
        else:
            self.save_backtest_selection()

        print('\n')