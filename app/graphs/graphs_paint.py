from .graphs_constructor import SimpleGraph, ComplexGraph, CustomGraph
from app.data.data_analyses import DataToGraphs
from app.data.strategy import StrategyToGraph
from config import ConfigGraphs


def save_plt(func):
    def wrapper(self, name, *args, **kwargs):
        graph = func(self, name, *args, **kwargs)
        path = self.path
        graph.savefig('%s/%s' % (path, name), dpi=int(ConfigGraphs().DPI), transparent = True)
        graph.clf()
        graph.close()
    return wrapper


class GraphPainter:
    def __init__(self, path, indicator_1, indicator_2, tickers):
        self.path = path
        self.DATA = DataToGraphs(tickers)
        self.DATA.set_indicator_1(indicator_1)
        self.DATA.set_indicator_2(indicator_2)


    def set_path(self, new_path):
        self.path = new_path


    @save_plt
    def indicators_hist(self, graph_name, *args, indicator, change_color = False):

        tickers, dfs = self.DATA.ind_complex_hist(*args, indicator = indicator)
        CG = ComplexGraph()

        if change_color:
            CG.set_colors('#0070c0')

        graph = CG.graph_one_ax(main_graph = 'hist',
                                table_list = dfs, 
                                title_list = tickers,
                                label_x = '$млн$ $руб.$', 
                                label_y = '$частота$',
                                date_index = False,
                                set_x_ticks = False,
                                ax_nums = 6)
        return graph


    @save_plt
    def indicators_scatter(self, graph_name, *args):

        tickers, dfs = self.DATA.ind_complex_scatter(*args)
        CG = ComplexGraph()

        graph = CG.graph_one_ax(main_graph = 'scatter',
                                table_list = dfs, 
                                title_list = tickers,
                                label_x = '$flow,$ $млн$ $руб.$', 
                                label_y = '$доходность,$ %',
                                date_index = False,
                                set_x_ticks = False,
                                ax_nums = 6)
        return graph

    
    @save_plt
    def correlations(self, graph_name, *args):

        tickers, dfs = self.DATA.correlation_complex(*args)
        CG = ComplexGraph()

        graph = CG.graph_one_ax(main_graph = 'line',
                                table_list = dfs, 
                                title_list = tickers,
                                label_x = '$размер$ $окна,$ $дней$', 
                                label_y = '$корреляция$',
                                date_index = False,
                                set_x_ticks = True,
                                add_legend = True,
                                add_ylim = 'correlation',
                                ax_nums = 6)
        return graph


    @save_plt
    def heatmap_corr(self, graph_name, indicator):
    
        df = self.DATA.get_indicators_corr(indicator)
        SG = SimpleGraph()

        graph = SG.graph_one_ax(main_graph = 'heatmap', 
                                df = df, 
                                label_x = None, 
                                label_y = None,
                                date_index = False,
                                set_x_ticks = False)

        return graph


    @save_plt
    def indicators_cum(self, graph_name, *args):
    
        tickers, dfs =  self.DATA.flow_cum_complex(*args)
        _, dfs2 = self.DATA.price_complex(*args)
        CG = ComplexGraph()

        graph = CG.graph_two_axes(main_graph = 'line_price',
                                second_graph = 'line_price', 
                                table_list_ax1 = dfs, 
                                table_list_ax2 = dfs2,
                                title_list = tickers,
                                label_x = '$год$', 
                                label_y = '$Накопленный$ $flow,$ $млрд$ $руб.$',
                                label_2y = '$цена,$ $руб.$',
                                set_x_ticks = False,
                                add_legend = True,
                                add_ylim = False,
                                add_ylim_2 = False,
                                ax_nums = 4)

        return graph

    
    @save_plt
    def backtest(self, graph_name, ticker):

        STG = StrategyToGraph(ticker, self.DATA.data)
        CG = CustomGraph()
        graph = CG.graph_for_backtest(table_list = STG.table_list,
                                        table_list_2 = STG.table_list_2,
                                        table_list_3 = STG.table_list_3,
                                        title_list = STG.title_list,
                                        set_x_ticks = False)
        
        return graph