import numpy as np
import pandas as pd
import warnings 
from typing import Union

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

from .graph_types import Graph, Line, Bar, Hystogram, Scatter, Heatmap, ScatterLine


class GraphTemplate:

    def __init__(self):
        self.Graph = Graph()
        self.colors = self.Graph.colors
        self.add_colors = self.Graph.add_colors
        self.lines = self.Graph.lines


    def set_colors(self, *args):
        new_colores = []
        
        for arg in args:
            new_colores.append(arg)
            
        self.colors = new_colores


    def set_add_colors(self, *args):
        new_colores = []
        
        for arg in args:
            new_colores.append(arg)
            
        self.add_colors = new_colores


    def set_lines(self, *args):
        new_lines = []
        
        for arg in args:
            new_lines.append(arg)
            
        self.lines = new_lines


    def format_ticks(self, ax):

        ax.tick_params (axis = 'x', 
                        labelsize = 8,
                        labelrotation = 45)


    def set_x_ticks(self, df, ax, is_date = False):

        ax.set_xticks(np.arange(len(df)))

        if is_date:
            ind = []
            for d in df.index:
                
                if d.month < 10:
                    m = str(0) + str(d.month)
                else:
                    m = str(d.month)

                ind.append('%s-%s' % (d.year, m))      
        else: 
            ind = list(df.index)
    
        ax.set_xticklabels(ind)


    def choose_format (self, data, data_type, round_num = 1):
        
        if data_type == 'int':
            return int(data)

        elif data_type == 'float':
            return round(data, round_num)

        elif data_type == 'interest':
            return '%s%%' % int(data)
        
        else:
            print('Choose int / float / interest data type')


    def add_annotations(self, df, ax, data_type, round_num = 1, ann_type = 'up'):
        
        for col, _ in enumerate(list(df.columns)):

            step = (ax.get_ylim()[1] - ax.get_ylim()[0]) / 15
            place = 0
            for ind, _ in enumerate(list(df.index)):
                score = df.iloc[ind, col]
                txt = self.choose_format(score, data_type)
            
                if ann_type == 'up':
                    place = score + step

                if ann_type == 'middle':
                    place = score / 2

                ax.annotate(txt, 
                            (ind, place), 
                            ha='center', 
                            va='center', 
                            size=8)

    
    def add_legend(self, df, ax, place, add_twinx = False):
        
        if add_twinx:
            box_place = 0.5
        else:
            box_place = 0

        if str(place)[-1] == '1':    
            ax.legend(tuple(df.columns)
                ,loc='upper left', 
                bbox_to_anchor=(box_place, 1.3), 
                ncol=2, facecolor='white', 
                edgecolor='white',
                fontsize = 12)


    def add_ylim(self, ax, ylim, df1 = None, df2 = None):
        if ylim == 'share':
            ax.set_ylim([0,100])

        elif ylim == 'correlation':
            ax.set_ylim([-1,1])

        elif ylim == 'vol':
            ax.set_ylim(bottom=0)

        elif ylim == 'max_y':
            up1 = df1.T.sum()
            up1 = np.max(up1)
            ax.set_ylim([0, up1 * 1.3])

        elif ylim == 'max_2y':
            up1 = df1.T.sum()
            up2 = df2.T.sum()

            up1 = np.max(up1)
            up2 = np.max(up2)

            ax.set_ylim([0, max(up1, up2) * 1.1])

        elif type(ylim) == int:
            ax.set_ylim([0,ylim])
        
        else: 
            pass


    def plot(self, graph_type, df, ax, colors):

        if graph_type == 'line':
            Line().template_1(df, ax, colors)

        if graph_type == 'line_price':
            Line().template_2(df, ax, colors)

        elif graph_type == 'stack':
            Bar().template_1(df, ax, colors)

        elif graph_type == 'hist':
            Hystogram().template_1(df, ax, colors)

        elif graph_type == 'scatter':
            Scatter().template_1(df, ax, colors)

        elif graph_type == 'line_scatter':
            ScatterLine().template_1(df, ax, colors)

        elif graph_type == 'heatmap':
            Heatmap().template_1(df, ax, colors)


    def graph_format(self, 
                    fig,
                    df,
                    df2 = None,
                    place = 111,
                    main_graph: str = 'line',
                    set_x_ticks: bool = False,
                    second_graph: Union[bool, str] = False,
                    label_x: Union[bool, str] = False, 
                    label_y: Union[bool, str] = False,
                    label_2y: Union[bool, str] = False,
                    date_index: bool = True,
                    add_legend: bool = False,
                    hide_2y: bool = False,
                    add_title: Union[bool, str] = False,
                    add_annotations: Union[bool, str] = False,
                    add_annotations_2y: Union[bool, str] = False,
                    add_ylim:  Union[bool, str] = False,
                    add_ylim_2: Union[bool, str] = False):


        ax = fig.add_subplot(place, sharex=None, sharey=None) 

        self.plot(main_graph, df, ax, self.colors)   
        self.format_ticks(ax)

        if set_x_ticks:
            self.set_x_ticks(df, ax, is_date=date_index)
        
        if label_x:    
            ax.set_xlabel(label_x)
        
        if label_y:
            ax.set_ylabel(label_y)

        if add_title:
            ax.set_title(add_title,loc = 'left')

        if add_annotations:
            self.add_annotations(df, ax, data_type = add_annotations, ann_type = 'up')

        if add_legend:
            self.add_legend(df, ax, place)

        if second_graph:
            ax2 = ax.twinx()
            self.plot(second_graph, df2, ax2, self.add_colors)
            ax2.grid(None)

            if hide_2y:
                ax2.axis('off')

            if label_2y:
                ax2.set_ylabel(label_2y)
            
            if add_ylim_2:
                self.add_ylim(ax, add_ylim, df, df2)
                self.add_ylim(ax2, add_ylim_2, df, df2)
            
            if add_legend:
                self.add_legend(df2, ax2, place, second_graph)

            if add_annotations_2y:
                self.add_annotations(df2, ax2, data_type = add_annotations_2y, ann_type = 'up')
        
        else:
            if add_ylim:
                self.add_ylim(ax, add_ylim, df)
