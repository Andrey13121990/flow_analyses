import asyncio
import aiohttp


class AsyncHttpLoader:

    def __init__(self, urls, file_abs_paths):
        self.urls = urls
        self.filepaths = file_abs_paths
            

    def save_file(self,
                data, 
                filepath: str) -> None:

        with open(filepath, 'wb') as file:
            file.write(data)


    async def fetch_data(self,
                        url: str, 
                        session: aiohttp.ClientSession,
                        filepath: str) -> None:

        async with session.get(url) as response:
            data = await response.read()
            self.save_file(data, filepath)


    async def download(self) -> None:

        tasks = []
        async with aiohttp.ClientSession() as session:
            for url, filepath in zip(self.urls, self.filepaths):
                task = asyncio.create_task(self.fetch_data(url, session, filepath))
                tasks.append(task)
            await asyncio.gather(*tasks)


    def start(self):
        asyncio.run(self.download())
