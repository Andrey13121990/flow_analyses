import os

from time import time
from typing import Union

from .urls import UrlFileFactory
from .httploader import AsyncHttpLoader


class ISS:

    def get_urls_list(self, tickers, year, url_name):
        urls = []
        paths = []
        
        for ticker in tickers:
            ISS = UrlFileFactory('.csv', ticker, year)
            urls.append(ISS.get_url(url_name))
            paths.append(ISS.get_path(url_name))

        return (urls, paths)


    def load_data(self, tickers, year, url_name):
        urls, paths = self.get_urls_list(tickers, year, url_name)
        AsyncHttpLoader(urls = urls, file_abs_paths = paths).start()


    def run_load(self, 
                year: str,
                eq_tickers: Union[bool, list] = False, 
                fx_tickers: Union[bool, list] = False,
                netflow_tickers: Union[bool, list] = False):
    
        t0 = time()

        if eq_tickers:
            self.load_data(eq_tickers, year, url_name = 'eq')
        
        if fx_tickers:
            self.load_data(fx_tickers, year, url_name = 'fx')
        
        if netflow_tickers:
            self.load_data(netflow_tickers, year, url_name = 'netflow')

        t1 = time() - t0
        print('Success quotes load %s year: %s sec.\n' % (year, round(t1, 2)))