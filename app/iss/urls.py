import datetime
import os

from config import ConfigDirectories, ConfigISS


class IssUrls:
    def __init__(self, file_format: str, ticker: str, year: str):
        self.file_format = file_format
        self.ticker = ticker
        self.year = year


    def get_date(self):
        today = datetime.datetime.today() 
        today_shift = today - datetime.timedelta(days=int(ConfigISS().delay))

        if str(self.year) == str(today.year):
            day = today_shift.strftime("-%m-%d")
        else:
            day = '-12-31'
        return day


    def dates_for_candles(self):
        dates = '{ticker}/candles{file_format}'\
                '?from={year}-01-01'\
                '&till={year}-12-31'\
                '&interval=24'
        return dates.format(file_format = self.file_format, 
                            ticker = self.ticker, 
                            year = self.year)


    def dates_for_flow(self):
        dates = '{ticker}{file_format}'\
                '?from={year}-01-01'\
                '&till={year}{day}'
        return dates.format(file_format = self.file_format, 
                            ticker = self.ticker, 
                            year = self.year, 
                            day = self.get_date())


    def get_eq_url(self):
        url = 'http://iss.moex.com/iss/engines/stock/markets/shares/securities/' 
        return url + self.dates_for_candles()


    def get_fx_url(self):        
        url = 'http://iss.moex.com/iss/engines/currency/markets/selt/boards/cets/securities/' 
        return url + self.dates_for_candles()


    def get_netflow_url(self):
        url = 'http://iss.moex.com/iss/analyticalproducts/netflow2/securities/'
        return url + self.dates_for_flow()



class FilesPath:
    def __init__(self, file_format: str, ticker: str, year: str):
        self.file_format = file_format
        self.ticker = ticker
        self.year = year


    def create_file_name(self):
        return '%s-%s%s' % (self.ticker, self.year, self.file_format)


    def create_path_to_save(self, directory):
        file_name = self.create_file_name()
        return os.path.join(os.getcwd(), directory, file_name)



class UrlFileFactory(IssUrls, FilesPath):
    def __init__(self, file_format: str, ticker: str, year: str):
        super().__init__(file_format, ticker, year)  
        self.factory = dict()
        self.create()


    def get_url(self, name):
        return self.factory[name]['url']


    def get_path(self, name):
        return self.factory[name]['path']


    def create_url_path(self, url_file_name, url, directory):
        abspath = self.create_path_to_save(directory)
        url_path = dict(url = url, path = abspath)
        self.factory[url_file_name] = url_path


    def create(self): 
        Dir = ConfigDirectories()

        self.create_url_path('eq', self.get_eq_url(), Dir.quotes), 
        self.create_url_path('fx', self.get_fx_url(), Dir.quotes),
        self.create_url_path('netflow', self.get_netflow_url(), Dir.netflow)