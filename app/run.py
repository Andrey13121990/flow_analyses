import datetime

from config import ConfigDirectories
from .iss.load import ISS
from .data.data_to_db import DatabsaseLoader
from .graphs.graphs_factory import SaveGraphs, timeit
from .structure.directories import Folders
from .structure.tickers import Tickers


class APP:

    def __init__(self):
        self.Folds = Folders()
        self.Folds.create_default_structure()
        self.Ticks = Tickers()


    def load_from_ISS(self, years):
        for year in years:
            ISS().run_load(year, 
                            eq_tickers = self.Ticks.get_eq_tickers(),
                            fx_tickers = self.Ticks.get_fx_tickers(), 
                            netflow_tickers = self.Ticks.get_netflow_tickers())


    def get_years(self, start: int):
        return [str(item) for item in range(start, datetime.date.today().year + 1)]


    def choose_years(self, period):
        if period == 'last_year':
            years = self.get_years(start = datetime.date.today().year)

        elif period == 'all':
            years = self.get_years(start = 2007)
        
        return years


    def upload_from_ISS(self, period):

        dirs = [ConfigDirectories().quotes, ConfigDirectories().netflow]
        self.Folds.clear_dir(*dirs)
        
        years = self.choose_years(period)
        self.load_from_ISS(years)

        DatabsaseLoader().load_data(period = period, directories = dirs) 


    def upload_i3_data(self):
        DatabsaseLoader().load_data(period = 'all', directories = [ConfigDirectories().indicators])


    def save_graphs(self):
        i3 = SaveGraphs(indicator_1 = 'i3', 
                        indicator_2 = 'i3_retail', 
                        tickers = self.Ticks.get_i3_tickers())

        nf = SaveGraphs(indicator_1 = 'pv100', 
                        indicator_2 = 'pv30', 
                        tickers = self.Ticks.get_netflow_tickers())

        i3.save(is_production = True)
        nf.save(is_production = True)


    @timeit('finish calculation --- total time')
    def run(self, 
            update_all = False, 
            update_last_year = True, 
            update_i3 = False, 
            graphs = True):
        
        if update_all:
            self.upload_from_ISS(period = 'all')

        if update_last_year:
            self.upload_from_ISS(period = 'last_year')
        
        if update_i3:
            self.upload_i3_data()

        if graphs:
            self.save_graphs()