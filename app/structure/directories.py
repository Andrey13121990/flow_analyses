import os
from config import ConfigDirectories

class Folders:
    def __init__(self):
        self.dirs = self.get_dirs()


    def get_dirs(self):
        dirs = ConfigDirectories().__dict__.values()
        return list(dirs)


    def make_dir(self, *folder_names: str, path: str = os.getcwd()):  
        for folder_name in folder_names:
            directory = os.path.join(path,folder_name)
            try:
                os.makedirs(directory)
            except FileExistsError:
                pass

    
    def clear_dir(self, *folder_names: str, path: str = os.getcwd()): 
        for folder_name in folder_names:
            directory = os.path.join(path,folder_name)

            try:
                for file in os.listdir(directory):
                    file_path = os.path.join(directory, file)
                    os.remove(file_path)
            except FileNotFoundError:
                pass


    def is_empty(self, path):
        
        if len(os.listdir(path)) == 0:
            raise FileNotFoundError('No data in folder %s' % path)
        else:
            pass

    
    def create_default_structure(self):
        for folder in self.dirs:
            self.make_dir(folder)
