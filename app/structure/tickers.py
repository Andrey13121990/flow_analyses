from config import ConfigTickers

class Tickers:

    def __init__(self):
        self.Tickers = ConfigTickers()


    def get_tickers(self, tickers):
        data = tickers.copy()
        data.sort()
        return data


    def get_i3_tickers(self):
        return self.get_tickers(self.Tickers.i3)


    def get_netflow_tickers(self):
        return self.get_tickers(self.Tickers.netflow)


    def get_fx_tickers(self):
        return self.get_tickers(self.Tickers.fx)


    def get_eq_tickers(self):
        t1 = self.get_i3_tickers()
        t2 = self.get_netflow_tickers()
        t1.extend(t2)
        result = list(set(t1))
        result.sort()
        return result