import configparser
import os


class Configuration:

    def get_data(self, section: str, parameter: str):
        config = configparser.ConfigParser()
        config_file = os.getcwd() + '/config.ini'
        config.read(config_file)
        
        return config[section].get(parameter)


class ConfigDirectories(Configuration):
    def __init__(self):
        super().__init__()
        self.quotes = self.get_data('DIRECTORIES', 'quotes')
        self.netflow = self.get_data('DIRECTORIES', 'netflow')
        self.indicators = self.get_data('DIRECTORIES', 'indicators')
        self.results = self.get_data('DIRECTORIES', 'results')
        

class ConfigDB(Configuration):
    def __init__(self):
        super().__init__()
        self.db = self.get_data('DATABASE', 'db')
        self.login = self.get_data('DATABASE', 'login')
        self.password = self.get_data('DATABASE', 'password')
        self.POSTGRES_URI = self.get_data('DATABASE', 'URI_POSTGRES')
        self.EXADATA_URI = self.get_data('DATABASE', 'URI_EXADATA')
        self.view_name = self.get_data('DATABASE', 'VIEW_NAME')


class ConfigGraphs(Configuration):
    def __init__(self):
        super().__init__()
        self.DPI = self.get_data('GRAPHS', 'DPI')


class ConfigISS(Configuration):
    def __init__(self):
        super().__init__()
        self.delay = self.get_data('ISS', 'delay')


class ConfigTickers(Configuration):
    def __init__(self):
        super().__init__()
        self.i3 = ['AFLT', 'ALRS', 'CHMF', 'GAZP', 'GMKN', 'LKOH', 'MAGN', 'MGNT', 'MOEX', 
                'ROSN', 'SBER', 'SNGS', 'SNGSP', 'VTBR', 'YNDX']

        self.fx = ['USD000UTSTOM', 'USD000000TOD']

        self.netflow = ['AFLT', 'ALRS', 'FEES', 'FIVE', 'GAZP', 'GMKN', 'HYDR', 'IRAO', 'LKOH', 
                        'MAGN', 'MGNT', 'MTLR', 'MTSS', 'NLMK', 'NVTK', 'RASP', 'ROSN', 'SBER', 
                        'SBERP', 'SIBN', 'SNGS', 'TATN', 'VTBR', 'YNDX']


class ConfigStrategy(ConfigTickers):
    def __init__(self):
        super().__init__()
        self.parametres = None
        self.set_parametres_production()
        

    def set_parametres_development(self, version):
        params = dict()
        for ticker in self.netflow:
            params[ticker] = self.get_parametres_development(version)
        self.parametres = params


    def set_parametres_production(self):
        params = dict()
        for ticker in self.netflow:
            params[ticker] = self.get_parametres_production(ticker)
        self.parametres = params


    def get_parametres_production(self, ticker):
        params = self.parametres_prod()
        return self.parametres_to_dict(params, ticker)


    def get_parametres_development(self, version):
        params = self.parametres_dev()
        return self.parametres_to_dict(params, version)


    def parametres_to_dict(self, dictionary, field):
        return dict(order = dictionary[field][0], 
                    shift = dictionary[field][1], 
                    ind = dictionary[field][2], 
                    ord_min = dictionary[field][3], 
                    ord_max = dictionary[field][4], 
                    shift_min = dictionary[field][5], 
                    shift_max = dictionary[field][6], 
                    mov_avg = dictionary[field][7])


    def parametres_prod(self):
        return {
            'VTBR' : [200, 30, 'pv100', 200, 200, 30, 30, 200],
            'SBER' : [180, 30, 'pv100', 180, 300, 30, 30, 50],
            'AFLT' : [180, 30, 'pv100', 300, 300, 30, 30, 80],
            'ALRS' : [200, 30, 'pv100', 200, 200, 30, 30, 100],
            'FEES' : [200, 30, 'pv100', 100, 200, 40, 40, 50],
            'FIVE' : [180, 30, 'pv100', 300, 300, 30, 30, 100],
            'GAZP' : [100, 40, 'pv100', 100, 400, 30, 30, 50], 
            'GMKN' : [400, 30, 'pv100', 400, 400, 30, 30, 50], 
            'HYDR' : [200, 30, 'pv100', 200, 200, 30, 30, 70], 
            'IRAO' : [180, 30, 'pv100', 300, 300, 30, 30, 100], 
            'LKOH' : [100, 30, 'pv100', 100, 400, 30, 30, 50],
            'MAGN' : [100, 40, 'pv100', 100, 400, 30, 30, 50], 
            'MGNT' : [100, 40, 'pv100', 400, 100, 40, 40, 100], 
            'MTLR' : [300, 30, 'pv100', 300, 300, 30, 30, 100], 
            'MTSS' : [300, 30, 'pv100', 300, 300, 30, 30, 100], 
            'NLMK' : [100, 40, 'pv100', 100, 400, 30, 30, 50],
            'NVTK' : [180, 30, 'pv100', 300, 300, 30, 30, 100], 
            'RASP' : [180, 30, 'pv100', 300, 300, 30, 30, 100], 
            'ROSN' : [100, 30, 'pv100', 100, 100, 30, 30, 50],
            'SBERP': [100, 40, 'pv100', 100, 200, 30, 30, 100], 
            'SIBN' : [300, 30, 'pv100', 300, 300, 30, 30, 100], 
            'SNGS' : [300, 30, 'pv100', 300, 300, 30, 30, 100], 
            'TATN' : [100, 40, 'pv100', 200, 100, 40, 40, 100],
            'YNDX' : [300, 30, 'pv100', 300, 300, 30, 30, 100]}  


    def parametres_dev(self):
        return {
            'pv70_v1_1' : [100, 30, 'pv70', 100, 100, 30, 30, 50],
            'pv70_v1_2' : [200, 30, 'pv70', 200, 200, 30, 30, 50],
            'pv70_v1_3' : [300, 30, 'pv70', 300, 300, 30, 30, 50],
            'pv70_v1_4' : [400, 30, 'pv70', 400, 400, 30, 30, 50],
            'pv70_v2_1' : [100, 30, 'pv70', 100, 100, 30, 30, 100],
            'pv70_v2_2' : [200, 30, 'pv70', 200, 200, 30, 30, 100],
            'pv70_v2_3' : [300, 30, 'pv70', 300, 300, 30, 30, 100],
            'pv70_v2_4' : [400, 30, 'pv70', 400, 400, 30, 30, 100],
            'pv70_v3_1' : [100, 30, 'pv70', 100, 100, 30, 30, 150],
            'pv70_v3_2' : [200, 30, 'pv70', 200, 200, 30, 30, 150],
            'pv70_v3_3' : [300, 30, 'pv70', 300, 300, 30, 30, 150],
            'pv70_v3_4' : [400, 30, 'pv70', 400, 400, 30, 30, 150],
            'pv70_v4_1' : [100, 30, 'pv70', 100, 100, 30, 30, 200],
            'pv70_v4_2' : [200, 30, 'pv70', 200, 200, 30, 30, 200],
            'pv70_v4_3' : [300, 30, 'pv70', 300, 300, 30, 30, 200],
            'pv70_v4_4' : [400, 30, 'pv70', 400, 400, 30, 30, 200],
            'pv70_v5_1' : [100, 40, 'pv70', 100, 200, 30, 30, 50],
            'pv70_v5_2' : [100, 40, 'pv70', 100, 400, 30, 30, 50],
            'pv70_v5_3' : [100, 40, 'pv70', 200, 100, 30, 30, 50],
            'pv70_v5_4' : [100, 40, 'pv70', 400, 100, 30, 30, 50],
            'pv70_v6_1' : [100, 40, 'pv70', 100, 200, 30, 30, 100],
            'pv70_v6_2' : [100, 40, 'pv70', 100, 400, 30, 30, 100],
            'pv70_v6_3' : [100, 40, 'pv70', 200, 100, 30, 30, 100],
            'pv70_v6_4' : [100, 40, 'pv70', 400, 100, 30, 30, 100],
            'pv70_v7_1' : [80, 40, 'pv70', 100, 200, 40, 40, 50],
            'pv70_v7_2' : [80, 40, 'pv70', 200, 400, 40, 40, 50],
            'pv70_v7_3' : [80, 40, 'pv70', 200, 100, 40, 40, 50],
            'pv70_v7_4' : [80, 40, 'pv70', 400, 100, 40, 40, 50],
            'pv70_v8_1' : [80, 40, 'pv70', 100, 200, 40, 40, 100],
            'pv70_v8_2' : [80, 40, 'pv70', 100, 400, 40, 40, 100],
            'pv70_v8_3' : [80, 40, 'pv70', 200, 100, 40, 40, 100],
            'pv70_v8_4' : [80, 40, 'pv70', 400, 100, 40, 40, 100]}


CONFIG_STRATEGY_CLOBAL = ConfigStrategy()